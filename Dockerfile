FROM ubuntu:18.04

RUN apt-get update \
	&& apt-get install --no-install-recommends --no-install-suggests -y python3.8;

RUN alias python=python3.8

#RUN python --version

#ENTRYPOINT ["python", "--version"]
